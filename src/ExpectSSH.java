/**
 * ExpectSSH; An Ant SSHExec task with definable responses to system prompts 
 *
 * ExpectSSH allows Ant scripts to react to system prompts with pre-defined responses.
 * This was primarily built to allow SSHExec tasks to respond to the sudo, or other such
 * password prompts. A prime example would be using SShExec to run an Ant build script on a
 * remote server which will request the sudo password, svn username and svn password:
 *
 *        <expectssh host="${SERVER}" username="${USERNAME}" password="${PASSWORD}" command="cd /home/test ; sudo ant build" usepty="true">
 *           <expect request="[sudo] password for ${USERNAME}:" response="${PASSWORD}"/>
 *           <expect request="SVN username:" response="${SVNUSER}"/>
 *           <expect request="SVN password:" response="${SVNPWD}"/>
 *       </expectssh> 
 *
 * Use this tool by placinng the .jar in a know location and importing it into your build script:
 *
 * <taskdef name="expectssh" classname="ExpectSSH" classpath=".. path to ... /ExpectSSH.jar"/>
 *
 * @author      Chris Gillespie <chris@elder-studios.co.uk>
 * @version     1.0
 * @since       2013-07-15
 */

import java.util.ArrayList;
import com.jcraft.jsch.ChannelExec;
import java.io.OutputStream;
import java.io.IOException;;
import org.apache.tools.ant.BuildException;

public class ExpectSSH extends AntSSHExec {

    private static final int RETRY_INTERVAL = 500;

    private ArrayList<Expect> expects = new ArrayList(); 

    protected Thread initThread(final ChannelExec channel, final OutputStream out) throws BuildException {
        try {
            return new ExpectSSHMonitor(channel, out, expects) ;
        } catch ( Exception e ) {
            throw new BuildException(e) ;
        }
    }

    private static class ExpectSSHMonitor extends Thread { 
        
        private ChannelExec channel ;
        private OutputStream out ;
        private ArrayList<Expect> expects ;
        private OutputStream sshCommandStream ;

        public ExpectSSHMonitor(ChannelExec channel, OutputStream out, ArrayList<Expect> expects)            
            throws IOException {
            this.channel = channel ;
            this.out = out ;
            this.expects = expects ;
            this.sshCommandStream = channel.getOutputStream();
        }

        public void run() {
            String lastRequest = "";

            while (!this.channel.isClosed()) {
                try {
                    String output = out.toString() ;
                    for ( Expect expect : this.expects ) {
                        if ( output.trim().endsWith(expect.request) && !lastRequest.equals(expect.request) ) {
                            lastRequest = expect.request ;

                            sshCommandStream.write((expect.response + "\n").getBytes()) ;
                            sshCommandStream.flush() ;
                        } else {
                            lastRequest = "" ;
                        }
                    }

                    sleep(RETRY_INTERVAL);
                } catch (Exception ignored) {}
            }
        }
    }

    public void addConfiguredExpect(Expect expect) {
        expects.add(expect.checkIsLegal()) ;
    }

    public static class Expect {

        private String request ;
        private String response ;

        public void setRequest(String request) { 
            this.request = request ; 
        }
        
        public void setResponse(String response) { 
            this.response = response ; 
        }

        public Expect checkIsLegal() {
            if ( this.request == null || this.response == null ) {
                throw new BuildException("Expect must have a request and a response attribute set") ;               
            }
            return this ;
        }
    }
}
